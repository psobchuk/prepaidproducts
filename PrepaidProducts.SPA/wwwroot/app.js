'use strict';

var prepaidProds = angular.module('yapp', ['ui.router', 'ngCookies', 'smoothScroll', 'slick', 'ngFileUpload']);

prepaidProds.constant('urls', { "apiUrl": "http://api.prods/api" });

prepaidProds.factory('tokenInterceptor', ['$q', '$location', '$cookies', function ($q, $location, $cookies) {
    return {
        request: function (config) {
            config.headers['X-Token'] = $cookies.get('token-cookie');
            return config;
        },

        responseError: function(rejection) {
            if (rejection.status === 401) {
                $location.path('/login');
                return $q.reject(rejection);
            }

            /* If not a 401, do nothing with this error.
             * This is necessary to make a `responseError`
             * interceptor a no-op. */
            return $q.reject(rejection);
        }
    }
}]);

prepaidProds.config(function ($httpProvider) {
    $httpProvider.defaults.headers.post['Content-Type']= 'application/json; charset=utf-8';
    $httpProvider.interceptors.push('tokenInterceptor');
});

prepaidProds.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('/dashboard', '/dashboard/overview');

    $stateProvider
    .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'views/base.html'
    })
    .state('login', {
        url: '/login',
        parent: 'base',
        templateUrl: 'views/login.html',
        controller: 'AuthCtrl'
    })
    .state('admin', {
        url: '/admin',
        parent: 'base',
        templateUrl: 'views/admin.html',
        controller: "AdminCtrl"
    })
    .state('add-vendor', {
        url: '/admin/addvendor',
        parent: 'base',
        templateUrl: 'views/addVendor.html',
        controller: "AdminCtrl"
    })
    .state('map', {
        url: '/map',
        parent: 'base',
        templateUrl: 'views/map.html',
        controller: "MapCtrl"
    })
    .state('link-product', {
        url: '/admin/linkproduct',
        parent: 'base',
        templateUrl: 'views/linkproduct.html',
        controller: "LinkCtrl"
    })
    .state('add-product', {
        url: '/admin/addproduct',
        parent: 'base',
        templateUrl: 'views/addProduct.html',
        controller: "AdminCtrl"
    });
});

prepaidProds.run(["$rootScope", "$state", "$cookies", function ($rootScope, $state, $cookies) {
    $rootScope.$state = $state;
    $rootScope.$cookies = $cookies;
}]);


prepaidProds.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);