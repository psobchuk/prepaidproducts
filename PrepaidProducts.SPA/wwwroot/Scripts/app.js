'use strict';

var prepaidProds = angular.module('yapp', ['ui.router']);

prepaidProds.constant('urls', { "apiUrl": "api.prods" });

prepaidProds.config(function ($httpProvider) {
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
});

prepaidProds.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('/dashboard', '/dashboard/overview');

    $stateProvider
    .state('base', {
        abstract: true,
        url: '',
        templateUrl: 'views/base.html'
    })
    .state('login', {
        url: '/login',
        parent: 'base',
        templateUrl: 'views/login.html',
    })
    .state('dashboard', {
        url: '/admin',
        parent: 'base',
        templateUrl: 'views/admin.html',

    })
});
