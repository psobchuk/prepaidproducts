﻿prepaidProds.factory('AdminSvc', ["$http", "urls", 'Upload', function ($http, urls, Upload) {
    var selectedVendor = {};
    var selectedProduct = {};
    var isEditMode = false;

    return {
        setEditMode: function(value) {
            isEditMode = value;
            if (!value) {
                selectedVendor = {};
                selectedProduct = {};
            }
        },

        getEditMode: function () {
            return isEditMode;
        },

        selectProduct: function(product) {
            selectedProduct = product;
        },

        getSelectedProduct: function() {
            return selectedProduct;
        },

        selectVendor: function(vendor) {
            selectedVendor = vendor;
        },

        getSelectedVendor: function() {
            return selectedVendor;
        },

        getProducts: function (fnSuccess, fnError) {
            $http.get(urls.apiUrl + '/products/load').success(fnSuccess).error(fnError);
        },

        addProduct: function (product, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/products/add', product).success(fnSuccess).error(fnError);
        },

        editProduct: function (product, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/products/edit', product).success(fnSuccess).error(fnError);
        },

        removeProduct: function (productId, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/products/remove?productId=' + productId, "").success(fnSuccess).error(fnError);
        },

        getVendors: function (fnSuccess, fnError) {
            $http.get(urls.apiUrl + '/vendors/load').success(fnSuccess).error(fnError);
        },

        addVendor: function (vendor, fnSuccess, fnError) {
            Upload.upload({
                url: urls.apiUrl + '/vendors/add',
                data: vendor
            }).then(fnSuccess, fnError);
        },

        editVendor: function(vendor, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/vendors/edit', vendor).success(fnSuccess).error(fnError);
        },

        removeVendor: function (vendorId, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/vendors/remove?vendorId=' + vendorId, "").success(fnSuccess).error(fnError);
        },

        getRatePlans: function(fnSuccess, fnError) {
            $http.get(urls.apiUrl + '/rateplans/load').success(fnSuccess).error(fnError);
        },

        addRatePlan: function(plan, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/rateplans/add', plan).success(fnSuccess).error(fnError);
        },

        removeRatePlan: function(planId, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/rateplans/remove?rateplanId='+ planId, "").success(fnSuccess).error(fnError);
        },

        editRatePlan: function (plan, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/rateplans/edit', plan).success(fnSuccess).error(fnError);
        },

        login: function (admin, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/account/login', admin).success(fnSuccess).error(fnError);
        },

        linkProduct: function (vendorId, prodId, fnSuccess, fnError) {
            var data = { vId: vendorId, pId: prodId };
            $http.post(urls.apiUrl + '/link/linkproduct', data).success(fnSuccess).error(fnError);
        },

        removeLink: function (vendorId, prodId, fnSuccess, fnError) {
            var data = { vId: vendorId, pId: prodId };
            $http.post(urls.apiUrl + '/link/removelink', data).success(fnSuccess).error(fnError);
        }
    }
}]);