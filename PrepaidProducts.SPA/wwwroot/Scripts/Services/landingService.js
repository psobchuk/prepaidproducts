﻿prepaidProds.factory('LandingSvc', ["$http", "urls", function ($http, urls) {
    return {
        getMapVendors: function (fnSuccess, fnError) {
            $http.get(urls.apiUrl + "/landing/getmapvendors").success(fnSuccess).error(fnError);
        }
    }
}]);