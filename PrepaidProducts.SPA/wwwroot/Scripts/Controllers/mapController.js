﻿prepaidProds.controller('MapCtrl', ['$scope', 'AdminSvc', function ($scope, AdminSvc) {
    $scope.vendors = [];
    $scope.$on('$viewContentLoaded', function (event) {
        AdminSvc.getVendors(
            function (result) {
                if (result.StatusCode == 200) {
                    $scope.vendors = result.Result;
                }
            },
            function (error) { console.error(error); }
        );
    });

}]);