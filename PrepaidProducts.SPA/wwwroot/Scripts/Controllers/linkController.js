﻿prepaidProds.controller('LinkCtrl', ['$scope', 'AdminSvc', function ($scope, AdminSvc) {
    $scope.products = [];
    $scope.lookupProds = [];

    $scope.vendor = {};
    
    
    AdminSvc.getProducts(
        function (result) {
            if (result.StatusCode == 200) {
                $scope.products = result.Result;

                for (var i = 0; i < $scope.products.length; i++) {
                    $scope.lookupProds[$scope.products[i].id] = $scope.products[i];
                }
            }
        },
        function (error) { console.error(error); }
    );

    $scope.vendor = AdminSvc.getSelectedVendor();
    $scope.vendor.products = [];

    $scope.addLink = function () {
        AdminSvc.linkProduct($scope.vendor.Id, $scope.selectedProdId,
            function (result) {
                if (result.StatusCode == 200) {
                    $scope.vendor.Products.push($scope.lookupProds[$scope.selectedProdId]);
                }
            },
            function (err) { console.error(err); }
        );
    };

    $scope.removeLink = function (productId, index) {
        AdminSvc.removeLink($scope.vendor.Id, productId,
            function (result) {
                $scope.vendor.Products.splice(index, 1);
            },
            function(err) { console.error(err); }
        );
    }
}]);