﻿prepaidProds.controller('AdminCtrl', ['$scope', 'AdminSvc', function ($scope, AdminSvc) {
    $scope.vendors = [];
    $scope.products = [];
    $scope.ratePlans = [];

    $scope.vendor = AdminSvc.getSelectedVendor();
    $scope.product = AdminSvc.getSelectedProduct();
    $scope.isEditMode = AdminSvc.getEditMode();

    $scope.loadData = function () {
        AdminSvc.setEditMode(false);
        AdminSvc.getVendors(
            function (result) {
                if (result.StatusCode == 200) {
                    $scope.vendors = result.Result;
                }
            },
            function (error) { console.error(error); }
        );

        AdminSvc.getRatePlans(
            function (result) {
                if (result.StatusCode == 200) {
                    $scope.ratePlans = result.Result;
                }
            },
            function (error) { console.error(error); }
        );

        AdminSvc.getProducts(
            function (result) {
                if (result.StatusCode == 200) {
                    $scope.products = result.Result;
                }
            },
            function (error) { console.error(error); }
        );
    }

    $scope.addVendor = function () {
        var vendor = $scope.vendor;
        $scope.vendor.file = $scope.file;
        AdminSvc.addVendor(vendor,
            function (result) {
                if (result.StatusCode == 200) {
                    $scope.vendors.push(result.Result);
                }
            },

            function (error) { console.error(error); }
        );
    }

    $scope.onRemoveVendor = function (vendorId, index) {
        if (confirm("Are you sure you want to delete this vendor?")) {
            $scope.removeVendor(vendorId, index);
        }
    }

    $scope.removeVendor = function (vendorId, index) {
        AdminSvc.removeVendor(vendorId,
            function (data) {
                if (data.StatusCode == 200) {
                    //remove item
                    $scope.vendors.splice(index, 1);
                } else {
                    console.log(data);
                }
            },
            function (error) { console.error(error); }
        );
    }

    $scope.onEditVendor = function (vendor) {
        AdminSvc.setEditMode(true);
        AdminSvc.selectVendor(vendor);
        $scope.$state.go('add-vendor');
    }

    $scope.editVendor = function () {
        AdminSvc.editVendor($scope.vendor,
            function (data) {
                if (data.StatusCode == 200) {
                    AdminSvc.setEditMode(false);
                    $scope.$state.go('admin');
                } else {
                    console.log(data);
                }
            },
            function (error) { console.error(error); }
        )
    }

    $scope.addRatePlan = function () {
        var ratePlan = $scope.ratePlan;

        AdminSvc.addRatePlan(ratePlan,
            function (result) {
                if (result.StatusCode == 200) {
                    $scope.ratePlans.push(result.Result);
                }
            },

            function (error) { console.error(error); }
        );
    };

    $scope.onRemoveRatePlan = function (ratePlanId, index) {
         if (confirm("Are you sure you want to delete this rate plan?")) {
             $scope.removeRatePlan(ratePlanId, index);
         }
     }

    $scope.removeRatePlan = function (ratePlanId, index) {
        AdminSvc.removeRatePlan(ratePlanId,
            function (data) {
                if (data.StatusCode == 200) {
                    //remove item
                    $scope.ratePlans.splice(index, 1);
                } else {
                    console.log(data);
                }
            },
            function (error) { console.error(error); }
        );
    };

    $scope.onEditRatePlan = function (ratePlan) {
         $scope.ratePlan = ratePlan;
         $scope.ratePlan.showForm = true;
     }

    $scope.editRatePlan = function () {
        AdminSvc.editRatePlan($scope.ratePlan,
            function (data) {
                if (data.StatusCode == 200) {
                    AdminSvc.setEditMode(false);
                } else {
                    console.log(data);
                }
            },
            function (error) { console.error(error); }
        )
    }

    $scope.addProduct = function () {
        var product = $scope.product;

        AdminSvc.addProduct(product,
            function (result) {
                if (result.StatusCode == 200) {
                    $scope.products.push(result.Result);
                }
            },

            function (error) { console.error(error); }
        );
    }

    $scope.onRemoveProduct = function (productId, index) {
        if (confirm("Are you sure you want to delete this product?")) {
            $scope.removeProduct(productId, index);
        }
    }

    $scope.removeProduct = function (productId, index) {
        AdminSvc.removeProduct(productId,
            function (data) {
                if (data.StatusCode == 200) {
                    //remove item
                    $scope.products.splice(index, 1);
                } else {
                    console.log(data);
                }
            },
            function (error) { console.error(error); }
        );
    }

    $scope.onEditProduct = function (product) {
        AdminSvc.setEditMode(true);
        AdminSvc.selectProduct(product);
        $scope.$state.go('add-product');
    }

    $scope.editProduct = function () {
        AdminSvc.editProduct($scope.product,
            function (data) {
                if (data.StatusCode == 200) {
                    AdminSvc.setEditMode(false);
                    $scope.$state.go('admin');
                } else {
                    console.log(data);
                }
            },
            function (error) { console.error(error); }
        )
    }

    $scope.onLinkProduct = function (vendor) {
        AdminSvc.selectVendor(vendor);
        $scope.$state.go('link-product');
    }

   if($scope.$state.$current.name == 'admin') $scope.loadData();
}]);