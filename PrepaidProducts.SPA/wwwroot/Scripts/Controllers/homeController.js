﻿prepaidProds.controller('HomeCtrl', ['$scope', 'LandingSvc', function ($scope, LandingSvc) {
    $scope.vendors = [];

    $scope.getMapPoints = function () {
        LandingSvc.getMapVendors(
            function (result) {
                if (result.StatusCode == 200) {
                    $scope.vendors = result.Result;
                    if (document.getElementById('map') != null)
                        $scope.renderMap();
                }
            },
            function (error) { console.log(error); }
        );
    }

    $scope.renderMap = function () {
        var styleArray = [{
                featureType: "all",
                stylers: [{ saturation: -80 } ]
            }, {
                featureType: "road.arterial",
                elementType: "geometry",
                stylers: [{ hue: "#00ffee" }, { saturation: 50 }]
            }, {
                featureType: "poi.business",
                elementType: "labels",
                stylers: [{ visibility: "off" }]
            }];

        var myLatLng = { lat: 50.4664432, lng: 30.5152335 };
        var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 16,
                styles: styleArray
            });

        var infowindow = new google.maps.InfoWindow();

        for (var i = 0; i < $scope.vendors.length; i++) {
            var vendor = $scope.vendors[i];
            myLatLng = { lat: Number(vendor.Latitude), lng: Number(vendor.Longitude) };
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: vendor.Name
            });

            makeInfoWindowEvent(map, infowindow, vendor.Name, marker);
        }

        function makeInfoWindowEvent(map, infowindow, contentString, marker) {
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            });
        }

    }

    window.initMap = $scope.getMapPoints;
}]);

//marker = new google.maps.Marker({
//    position: { lat: 50.4652522, lng: 30.5163268 },
//    map: map,
//    title: 'Hello World!'
//});
//marker = new google.maps.Marker({
//    position: { lat: 50.4654447, lng: 30.5132491 },
//    map: map,
//    title: 'Hello World!'
//});
//marker = new google.maps.Marker({
//    position: { lat: 50.4657179, lng: 30.5110068 },
//    map: map,
//    title: 'Hello World!'
//});