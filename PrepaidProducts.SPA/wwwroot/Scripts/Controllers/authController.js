﻿prepaidProds.controller('AuthCtrl', [ '$scope', 'AdminSvc', function ($scope, AdminSvc) {
    $scope.admin = {};
    $scope.token = "";

    $scope.login = function () {
        AdminSvc.login($scope.admin,
            function (data) {
                if (data.StatusCode === 200) {
                    $scope.$cookies.put('token-cookie', data.Result);
                } else {
                    console.log(data);
                }
            },
            function (error) { console.error(error); }
        );
    }
}]);