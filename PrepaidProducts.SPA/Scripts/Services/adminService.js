﻿prepaidProds.factory('AdminSvc', ["$http", function ($http) {
    return {
        getVendors: function (fnSuccess, fnError) {
            $http.get(urls.apiUrl + '/vendors/load').success(fnSuccess).error(fnError);
        },

        getProducts: function (fnSuccess, fnError) {
            $http.get(urls.apiUrl + '/products/load').success(fnSuccess).error(fnError);
        }
    }
}]);