﻿using PrepaidProducts.DataAccess.DataTransferObjects;
using PrepaidProducts.DataAccess.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrepaidProducts.Controllers
{
    public class ProductsController : BaseAppController
    {
        private IProductManager productManager;

        public ProductsController(IProductManager manager)
        {
            productManager = manager;
        }

        [HttpGet]
        public IHttpActionResult Load()
        {
            ICollection<ProductDTO> products = productManager.GetAll();
            return Ok(products);
        }
        [HttpPost]
        public IHttpActionResult Add(ProductDTO product)
        {
            product.Id = productManager.AddProduct(product);
            return Ok(product);
        }

        [HttpPost]
        public IHttpActionResult Edit(ProductDTO product)
        {
            productManager.UpdateProduct(product);
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Remove(int productId)
        {
            productManager.DeleteProduct(productId);
            return Ok();
        }
    }
}
