﻿using PrepaidProducts.AppCode;
using PrepaidProducts.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Http.Results;

namespace PrepaidProducts.Controllers
{
    [GenericExceptionFilter]
    public class BaseAppController : ApiController
    {
        private CustomHttpActionResult httpActionResult;
        private static HttpClient client;

        public HttpClient BaseHttpClient
        {
            get
            {
                if (client == null)
                {
                    client = new HttpClient();
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["AppUrl"].ToString());
                }

                return client;
            }
        }

        public int CurrentUserId
        {
            get
            {
                var id = Convert.ToInt32(User.Identity.Name);
                return id;
            }
        }

        public long PersonalMagId
        {
            get
            {
                var identity = User.Identity as GenericIdentity;
                Claim claim = identity.Claims.Where(c => c.Type == "PersonalMagId").FirstOrDefault();
                if (claim == null || string.IsNullOrEmpty(claim.Value))
                {
                    Logger.LogDebug("No PersonalMagId claim found for user");
                    throw new NullReferenceException("No PersonalMagId claim found for user.");
                }

                return Convert.ToInt64(claim.Value);
            }
        }

        public string CurrentDeviceID
        {
            get
            {
                if (Request.Headers.Contains("X-Token"))
                {
                    var encryptedToken = Request.Headers.GetValues("X-Token").First();
                    Token token = Token.Decrypt(encryptedToken);
                    return token.DeviceID;
                }

                return string.Empty;
            }
        }

        public BaseAppController()
        {
            httpActionResult = new CustomHttpActionResult();
            httpActionResult.RequestMessage = Request;
        }

        #region Response methods

        /// <summary>
        /// Creates CustomHttpActionResult with 200 OK status code
        /// </summary>
        /// <param name="responseBody">Optional content body response</param>
        /// <returns>CustomHttpActionResult object</returns>
        public IHttpActionResult Ok(object responseBody = null)
        {
            return CreateHttpActionResult(HttpStatusCode.OK, responseBody);
        }

        /// <summary>
        /// Creates CustomHttpActionResult with 400 Bad Request status code
        /// </summary>
        /// <param name="responseBody">Optional content body response</param>
        /// <returns>CustomHttpActionResult object</returns>
        public IHttpActionResult BadRequest(object responseBody = null)
        {
            return CreateHttpActionResult(HttpStatusCode.BadRequest, responseBody);
        }

        /// <summary>
        /// Creates CustomHttpActionResult with 409 Conflict status code
        /// </summary>
        /// <param name="responseBody">Optional content body response</param>
        /// <returns>CustomHttpActionResult object</returns>
        public IHttpActionResult Conflict(object responseBody = null)
        {
            return CreateHttpActionResult(HttpStatusCode.Conflict, responseBody);
        }

        /// <summary>
        /// Creates CustomHttpActionResult with 500 Internal server error status code
        /// </summary>
        /// <param name="responseBody">Optional content body response</param>
        /// <returns>CustomHttpActionResult object</returns>
        public IHttpActionResult InternalServerError(object responseBody = null)
        {
            return CreateHttpActionResult(HttpStatusCode.InternalServerError, responseBody);
        }

        /// <summary>
        /// Creates CustomHttpActionResult with 401 Unauthorized status code
        /// </summary>
        /// <param name="responseBody">Optional content body response</param>
        /// <returns>CustomHttpActionResult object</returns>
        public IHttpActionResult Unauthorized(object responseBody = null)
        {
            return CreateHttpActionResult(HttpStatusCode.Unauthorized, responseBody);
        }

        /// <summary>
        /// Create generic http action result object
        /// </summary>
        /// <param name="code">Http status code</param>
        /// <param name="responseBody">Content body response</param>
        /// <returns>HttpActionResult response</returns>
        private IHttpActionResult CreateHttpActionResult(HttpStatusCode code, object responseBody)
        {
            httpActionResult.Code = code;
            httpActionResult.ResponseMessage = responseBody;

            return httpActionResult;
        }

        #endregion
    }
}
