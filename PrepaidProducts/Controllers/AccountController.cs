﻿using PrepaidProducts.AppCode;
using PrepaidProducts.Common;
using PrepaidProducts.DataAccess.DataTransferObjects;
using PrepaidProducts.DataAccess.Managers;
using PrepaidProducts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrepaidProducts.Controllers
{
    public class AccountController : BaseAppController
    {
        private IUserManager userManager;
        private IHttpActionResult actionResult;

        public AccountController(IUserManager manager)
        {
            this.userManager = manager;
        }

        [HttpPost]
        public IHttpActionResult Login(UserLoginViewModel model)
        {
            var user = userManager.GetUserByEmail(model.Email);
            if(user != null)
            {
                string password = MD5Hash.CreateMD5Hash(model.Password);
                if(user.Password.Equals(password))
                {
                    actionResult = Ok(new Token(user.Id, DateTime.Now, TimeSpan.FromTicks(0)).Encrypt());
                }
                else
                {
                    actionResult = BadRequest("User not found");
                }
            }
            else
            {
                actionResult = BadRequest("User not found");
            }

            return actionResult;
        }

        [HttpPost]
        public IHttpActionResult Register(UserRegisterViewModel model)
        {
            var user = new UserDTO()
            {
                Email = model.Email,
                Password = MD5Hash.CreateMD5Hash(model.Password),
                FullName = model.FullName
            };

            userManager.InsertUser(user);

            return Ok();
        }

        [HttpPost]
        public IHttpActionResult ForgotPassword(string email)
        {
            var user = userManager.GetUserByEmail(email);
            if (user != null)
            {
                //TODO: send email with password recovery? temporary password
            }
                
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult ChangePassword(string password)
        {
            //TODO: reset password
            userManager.GetUser(CurrentUserId);

            return Ok();
        }
    }
}
