﻿using PrepaidProducts.DataAccess.DataTransferObjects;
using PrepaidProducts.DataAccess.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrepaidProducts.Controllers
{
    public class LandingController : BaseAppController
    {
        private IVendorManager vendorManager;

        public LandingController(IVendorManager vendorManager)
        {
            this.vendorManager = vendorManager;
        }

        [HttpGet]
        public IHttpActionResult GetMapVendors()
        {
            ICollection<VendorDTO> vendors = vendorManager.GetAll();
            return Ok(vendors);
        }
    }
}
