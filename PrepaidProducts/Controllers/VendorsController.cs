﻿using PrepaidProducts.AppCode;
using PrepaidProducts.DataAccess.DataTransferObjects;
using PrepaidProducts.DataAccess.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PrepaidProducts.Controllers
{
    public class VendorsController : BaseAppController
    {
        private IVendorManager vendorManager;

        public VendorsController(IVendorManager manager)
        {
            vendorManager = manager;
        }

        [HttpGet]
        public IHttpActionResult Load()
        {
            ICollection<VendorDTO> vendors = vendorManager.GetAll();
            vendors.ToList().ForEach(v => v.ImageUrl = WebUtils.GetImageUrl(v.CoverImageName, true));

            return Ok(vendors);
        }

        [HttpGet]
        public IHttpActionResult GetOneVendor(int vendorId)
        {
            VendorDTO vendor = vendorManager.GetOne(vendorId);

            return Ok(vendor);
        }

        [HttpGet]
        public IHttpActionResult GetVendorCode(int vendorId)
        {
            return Ok();
        }

        public async Task<IHttpActionResult> Add()
        {
            try
            {
                string root = HttpContext.Current.Server.MapPath("~/FileStorage");
                var provider = new CustomMultipartFormDataProvider(root);
                // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

              


                VendorDTO vendor = new VendorDTO()
                {
                    Address = provider.FormData.Get("address"),
                    Description = provider.FormData.Get("description"),
                    Latitude = provider.FormData.Get("latitude"),
                    Longitude = provider.FormData.Get("longitude"),
                    Name = provider.FormData.Get("name")
                };

                // This illustrates how to get the file names.
                foreach (MultipartFileData file in provider.FileData)
                {
                    vendor.CoverImageName = file.LocalFileName.Substring(file.LocalFileName.LastIndexOf("\\") + 1);
                }

                //create thumb from image
                WebUtils.CreateThumbnailImage(vendor.CoverImageName);

                vendorManager.AddVendor(vendor);

            }
            catch (System.Exception e) { }

            return Ok();
        }


        [HttpPost]
        public IHttpActionResult Edit(VendorDTO vendor)
        {
            vendorManager.UpdateVendor(vendor);
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Remove(int vendorId)
        {

            VendorDTO vendor = vendorManager.GetOne(vendorId);

            vendorManager.DeleteVendor(vendorId);

            var filePath = HttpContext.Current.Server.MapPath("~/FileStorage/" + vendor.CoverImageName);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            return Ok();
        }

        [HttpGet]
        public IHttpActionResult Search(string sq)
        {
            var vendors = vendorManager.Search(sq);
            return Ok(vendors);
        }
    }
}
