﻿using PrepaidProducts.DataAccess.Managers;
using PrepaidProducts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrepaidProducts.Controllers
{
    public class LinkController : BaseAppController
    {
        private IProductManager productManager;
        private IVendorManager vendorManager;

        public LinkController(IProductManager prodManager, IVendorManager vendorManager)
        {
            this.productManager = prodManager;
            this.vendorManager = vendorManager;
        }

        [HttpPost]
        public IHttpActionResult LinkProduct(LinkRequestModel model)
        {
            var vendor = vendorManager.GetOne(model.VId);
            var product = productManager.GetOne(model.PId);

            vendorManager.LinkProduct(vendor, product);

            return Ok();
        }

        [HttpPost]
        public IHttpActionResult RemoveLink(LinkRequestModel model)
        {
            vendorManager.RemoveLink(model.VId, model.PId);

            return Ok();
        }
    }
}
