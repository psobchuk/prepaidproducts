﻿using PrepaidProducts.DataAccess.DataTransferObjects;
using PrepaidProducts.DataAccess.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrepaidProducts.Controllers
{
    public class RatePlansController : BaseAppController
    {
        private IRatePlanManager rpManager;

        public RatePlansController(IRatePlanManager manager)
        {
            rpManager = manager;
        }

        [HttpGet]
        public IHttpActionResult Load()
        {
            var data = rpManager.GetAll();
            return Ok(data);
        }

        [HttpPost]
        public IHttpActionResult Edit(RatePlanDTO ratePlan)
        {
            var data = rpManager.UpdateRatePlan(ratePlan);
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Remove(int ratePlanId)
        {
            rpManager.DeleteRatePlan(ratePlanId);
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Add(RatePlanDTO ratePlan)
        {
            rpManager.AddRatePlan(ratePlan);
            return Ok();
        }
    }
}
