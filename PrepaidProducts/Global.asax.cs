﻿using Castle.Windsor;
using PrepaidProducts.App_Start;
using PrepaidProducts.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PrepaidProducts
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private IWindsorContainer Container { get; set; }

        protected void Application_Start()
        {
            Container = new WindsorContainer();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Windsor composition root
            GlobalConfiguration.Configuration.Services.Replace(
            typeof(IHttpControllerActivator), new WindsorCompositionRoot(this.Container));
            this.Container.Install(new DependencyConventions());

            AutoMapperConfig.ConfigureMappings();


        }
    }
}
