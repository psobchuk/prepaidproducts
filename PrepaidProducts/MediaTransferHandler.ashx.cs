﻿using PrepaidProducts.AppCode;
using PrepaidProducts.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PrepaidProducts
{
    /// <summary>
    /// Сводное описание для MediaTrasnferHandler
    /// </summary>
    public class MediaTransferHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.HttpMethod == "GET")
            {
                HandleGet(context);
            }
            else
            {
                context.Response.ClearHeaders();
                context.Response.StatusCode = 405;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private void HandleGet(HttpContext context)
        {
            if (context.Request["f"] == null)
            {
                context.Response.StatusCode = 400; // Bad Request
                return;
            }

            string path = context.Request["f"].UrlDecoded();
            // TODO: use path from config instead of HostingEnvironment.ApplicationPhysicalPath
            string filePath = context.Server.MapPath("~/FileStorage/" + path);

            // Log debug info if debug flag is in the request
            if (context.Request["dbg"] != null && context.Request["dbg"] == "1")
            {
                Logger.LogDebug(string.Format("FileTransferHandler.HandleGet(DEBUG): original param = {0}, path = {1}", context.Request["f"], path));
            }

            if (File.Exists(filePath))
            {
                SetFileToResponse(context, filePath);
            }
            else
            {
                CheckDefaults(context);
            }
        }

        private void SetFileToResponse(HttpContext context, string filePath)
        {
            var imageFile = new FileInfo(filePath);
            if (!string.IsNullOrEmpty(context.Request.Headers["If-Modified-Since"]))
            {
                var provider = System.Globalization.CultureInfo.InvariantCulture;
                var lastMod = DateTime.ParseExact(context.Request.Headers["If-Modified-Since"], "r", provider);
                if (imageFile.LastWriteTimeUtc.AddSeconds(-1) <= lastMod)
                {
                    context.Response.StatusCode = 304;
                    context.Response.StatusDescription = "Not Modified";
                    return;
                }
            }

            context.Response.ContentType = WebUtils.GetMimeType(Path.GetExtension(imageFile.Name));
            context.Response.WriteFile(filePath);
            context.Response.Cache.SetCacheability(HttpCacheability.Public);
            context.Response.Cache.SetLastModified(imageFile.LastWriteTimeUtc);
        }

        private void CheckDefaults(HttpContext context)
        {
            var filepath = "~/FileStorage/sample.jpg";

            context.Response.ContentType = "image/jpeg";
            context.Response.WriteFile(context.Server.MapPath(filepath));
        }
    }
}