﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PrepaidProducts.Controllers;
using PrepaidProducts.DataAccess;
using PrepaidProducts.DataAccess.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrepaidProducts.IoC
{
    public class DependencyConventions : IWindsorInstaller
    {
        private string ConnectionString { get; set; }

        /// 
        /// Initialize WebInstaller
        /// 
        public DependencyConventions()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                            .Where(Component.IsInSameNamespaceAs<HomeController>())
                            .WithService.DefaultInterfaces()
                            .LifestyleTransient());

            container.Register(Component.For<UnitOfWork>().LifestylePerWebRequest());

            container.Register(Component.For<IUserManager>().ImplementedBy<UserManager>().LifestylePerWebRequest());
            container.Register(Component.For<IProductManager>().ImplementedBy<ProductManager>().LifestylePerWebRequest());
            container.Register(Component.For<IVendorManager>().ImplementedBy<VendorManager>().LifestylePerWebRequest());
            container.Register(Component.For<IRatePlanManager>().ImplementedBy<RatePlanManager>().LifestylePerWebRequest());
        }
    }
}