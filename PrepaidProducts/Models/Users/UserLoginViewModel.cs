﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrepaidProducts.Models
{
    public class UserLoginViewModel
    {
        //local log in
        public string Email { get; set; }
        public string Password { get; set; }
        //social networks login verification
        public string ExternalId { get; set; }
    }
}