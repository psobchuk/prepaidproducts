﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrepaidProducts.Models
{
    public class UserRegisterViewModel
    {
        //local register data
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FullName { get; set; }
        //in case if registered via  social networks
        public string ExternalLoginId { get; set; }
    }
}