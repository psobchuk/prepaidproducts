﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using PrepaidProducts.AppCode;
using System.Web.Http.Dispatcher;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;

namespace PrepaidProducts
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //// Конфигурация и службы Web API
            //// Настройка Web API для использования только проверки подлинности посредством маркера-носителя.
            //config.SuppressDefaultHostAuthentication();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            var tokenInspector = new TokenInspector
            {
                InnerHandler = new HttpControllerDispatcher(config)
            };

            // Маршруты Web API
            config.MapHttpAttributeRoutes();

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.Add(new FormUrlEncodedMediaTypeFormatter());

            config.Routes.MapHttpRoute(
                name: "login",
                routeTemplate: "api/account/login",
                defaults: new { controller = "account", action = "login" }
            );

            config.Routes.MapHttpRoute(
                name: "landing",
                routeTemplate: "api/landing/{action}",
                defaults: new { controller = "landing" }
            );

            config.Routes.MapHttpRoute(
                name: "API Default",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: null,
                handler: tokenInspector
            );
        }
    }
}
