﻿using PrepaidProducts.AppCode;
using System.Web;
using System.Web.Mvc;

namespace PrepaidProducts
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new GenericExceptionFilterAttribute());
        }
    }
}
