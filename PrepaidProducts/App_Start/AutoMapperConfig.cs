﻿using AutoMapper;
using PrepaidProducts.DataAccess.DataTransferObjects;
using PrepaidProducts.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrepaidProducts.App_Start
{
    public static class AutoMapperConfig
    {
        public static void ConfigureMappings()
        {
            Mapper.CreateMap<Vendor, VendorDTO>();
            Mapper.CreateMap<VendorDTO, Vendor>();

            Mapper.CreateMap<Product, ProductDTO>();
            Mapper.CreateMap<ProductDTO, Product>();

            Mapper.CreateMap<RatePlan, RatePlanDTO>();
            Mapper.CreateMap<RatePlanDTO, RatePlan>();

            Mapper.CreateMap<User, UserDTO>();
            Mapper.CreateMap<UserDTO, User>();
        }
    }
}