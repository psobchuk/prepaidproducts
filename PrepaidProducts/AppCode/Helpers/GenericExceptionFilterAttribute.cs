﻿using PrepaidProducts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace PrepaidProducts.AppCode
{
	/// <summary>
	/// Global generic exception filter
	/// Handles any exception that rises in api methods and returns (500 internal server error) response
	/// </summary>
	public class GenericExceptionFilterAttribute : ExceptionFilterAttribute
	{
		public override void OnException(HttpActionExecutedContext context)
		{
			HttpActionDescriptor descriptor = context.ActionContext.ActionDescriptor;
            Logger.LogErrorException(string.Format("GenericExceptionFilterAttribute - {0} - {1}:", descriptor.ControllerDescriptor.ControllerName, descriptor.ActionName), context.Exception);

			var message = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            message.Content = CustomHttpActionResult.CreateResponseContent(HttpStatusCode.InternalServerError, "Oops, internal server error appeared!");
			context.Response = message;
		}
	}
}