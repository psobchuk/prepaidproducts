﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace PrepaidProducts.AppCode
{
	/// <summary>
	/// Extension class for HttpRequestMessage object
	/// </summary>
    public static class HttpRequestMessageHelper
    {
		/// <summary>
		/// Get requestor ip address
		/// </summary>
		/// <param name="request">Current request message</param>
		/// <returns>Client IP Address</returns>
        public static string GetClientIpAddress(this HttpRequestMessage request)
        {
            //var ipAddress = string.Empty;

            //try
            //{
            //	if (request.Properties.ContainsKey("MS_HttpContext"))
            //		ipAddress = ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;

            //	if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            //		ipAddress = ((RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name]).Address;
            //}
            //catch (Exception ex)
            //{
            //	Logger.LogErrorException("GetClientIPAddress() - Unable to retrieve client ip address", ex);
            //}

            //         return ipAddress;

            return string.Empty;
        }
    }

    public static class StringExtensions
    {
        public static string UrlDecoded(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }

            //knockout fix here
            if (str.IndexOf("?f=") > -1) str = str.Substring(str.IndexOf("?f=") + 3).Split('&')[0];

            if (str.Contains("Content/")) return str;

            var bytes = HttpServerUtility.UrlTokenDecode(str);
            var chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);

            return new string(chars);
        }

        public static string UrlEncoded(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }

            var bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return HttpServerUtility.UrlTokenEncode(bytes);
        }
    }
}