﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;

namespace PrepaidProducts.AppCode
{
	/// <summary>
	/// Base action result class for mobile api methods
	/// Each mobile method result should contain proper http status code and optional response content body
	/// Current class objects are returned from each mobile api method
	/// </summary>
	public class CustomHttpActionResult: IHttpActionResult
	{
		public HttpStatusCode Code { get; set; }
		public object ResponseMessage { get; set; }
		public HttpRequestMessage RequestMessage { get; set; }

		public CustomHttpActionResult() { }

		public CustomHttpActionResult(HttpStatusCode code, object responseBody, HttpRequestMessage requestMessage)
		{
			Code = code;
			ResponseMessage = responseBody;
			RequestMessage = requestMessage;
		}

		/// <summary>
		/// Transforms action result input data into response message
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns>Predefined mobile api response message</returns>
		public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
		{
			//Create mobile api response
			var responseMessage = new HttpResponseMessage(Code);
			responseMessage.RequestMessage = RequestMessage;
			responseMessage.Content = CreateResponseContent(Code, ResponseMessage);

			return Task.FromResult(responseMessage);
		}

		/// <summary>
		///  Create generic mobile api response body content
		/// </summary>
		/// <param name="code">Http status code</param>
		/// <param name="responseMessage">Response body message</param>
		/// <returns>String content object</returns>
		public static StringContent CreateResponseContent(HttpStatusCode code, object responseMessage)
		{
			return new StringContent(new MobileApiResponse(code, responseMessage).ToString());
		}

		/// <summary>
		/// Base mobile api response class
		/// Represents generic mobile api response
		/// Used internally by custom http response
		/// </summary>
		class MobileApiResponse
		{
			public HttpStatusCode StatusCode { get; set; }
			public object Result { get; set; }

			public MobileApiResponse(HttpStatusCode code, object result)
			{
				StatusCode = code;
				Result = result;
			}

			public override string ToString()
			{
				return JsonConvert.SerializeObject(this);
			}
		}
	}
}

