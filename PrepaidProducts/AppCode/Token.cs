﻿using PrepaidProducts.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrepaidProducts.AppCode
{
	/// <summary>
	/// Token class represents user client token which grants access to mobile api methods
	/// Token is obligatory fore each client request
	/// </summary>
    public class Token
    {
        public long UserId { get; private set; }
        public TimeSpan Duration { get; private set; }
        public DateTime Created { get; private set; }
		public string DeviceID { get; set; }
		public long PersonalMagId { get; set; }

        public Token(long id, DateTime createdAt, TimeSpan tokenDuration)
        {
            UserId = id;
            Created = createdAt;
            // if tokenDuration is not specified than token is permanent
            Duration = tokenDuration;
        }

		/// <summary>
		/// Validate user token expiration date
		/// </summary>
		/// <returns>True if token is expired</returns>
        public bool Expired()
        {
            if (Duration.Ticks == 0)
            {
                return false;
            }
            return this.Created + this.Duration < DateTime.UtcNow;
        }

		/// <summary>
		/// Stringify current token values
		/// </summary>
		/// <returns></returns>
        public override string ToString()
        {
            return string.Format("UserId={0};Created={1};Duration={2};", UserId, Created.Ticks, Duration.Ticks);
        }

		/// <summary>
		/// Encrypt current token
		/// </summary>
		/// <returns>encrypted token string</returns>
        public string Encrypt()
        {
            return Encryption.EncryptString(this.ToString());
        }

		/// <summary>
		/// Get token object from encrypted string
		/// </summary>
		/// <param name="encryptedToken">token string</param>
		/// <returns>token object</returns>
        public static Token Decrypt(string encryptedToken)
        {
            var decrypted = Encryption.DecryptString(encryptedToken);
            var properties = decrypted.Split(';');

            var id = long.Parse(properties[0].Replace("UserId=", ""));
            var created = new DateTime(long.Parse(properties[1].Replace("Created=", "")));
            var duration = new TimeSpan(long.Parse(properties[2].Replace("Duration=", "")));

            return new Token(id, created, duration);
        }
    }
}