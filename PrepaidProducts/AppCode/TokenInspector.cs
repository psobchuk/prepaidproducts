﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Security.Principal;
using PrepaidProducts.Common;

namespace PrepaidProducts.AppCode
{
	/// <summary>
	/// Token inspector is injected in route configuration and checks each request for valid token presence
	/// Starts before controller invocation process
	/// </summary>
    public class TokenInspector: DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            const string TOKEN_NAME = "X-Token";
            if (request.Headers.Contains(TOKEN_NAME))
            {
                var encryptedToken = request.Headers.GetValues(TOKEN_NAME).First();
                try
                {
                    Token token = Token.Decrypt(encryptedToken);
                    //make sure that token is still valid and came from authenticated user's ip address
                    if (!token.Expired())
                    {
                        //Create generic user identity to specify unique user id and personal mag id
						//Generic identity is accessed via mobile api methods
						GenericIdentity identity = new GenericIdentity(token.UserId.ToString());
						identity.AddClaim(new System.Security.Claims.Claim("PersonalMagId", token.PersonalMagId.ToString()));
						HttpContext.Current.User = new GenericPrincipal(identity, new string[] {""});
                    }
                    else
                    {
                        //Logger.LogWarn("TokenInspector - invalid token");
						return CreateResponseMessage(HttpStatusCode.Unauthorized, "Invalid Token.");
                    }
                }
                catch(Exception ex)
                {
					Logger.LogErrorException("TokenInspector - invalid token:", ex);
					return CreateResponseMessage(HttpStatusCode.Unauthorized, "Invalid Token.");
                }
            }
            else
            {
                Logger.LogWarn("TokenInspector - Request is missing authorization token. request.RequestUri = " + request.RequestUri);
				return CreateResponseMessage(HttpStatusCode.Unauthorized, "Request is missing authorization token.");
            }

            return base.SendAsync(request, cancellationToken);
        }

		private Task<HttpResponseMessage> CreateResponseMessage(HttpStatusCode code, string message)
		{
			var responseMessage = new HttpResponseMessage(code);
			responseMessage.Content = CustomHttpActionResult.CreateResponseContent(code, message);

			return Task.FromResult(responseMessage);
		}
    }
}