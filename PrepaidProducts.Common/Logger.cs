﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PrepaidProducts.Common
{
    /// <summary>
    ///     Basic functionality for logging purposes, using NLog
    /// </summary>
    public sealed class Logger
    {
        private static NLog.Logger _logger;

        /// <summary>
        ///     Default constructor
        /// </summary>
        private Logger()
        {
        }

        /// <summary>
        ///     An instance of a NLog Logger.
        /// </summary>
        private static NLog.Logger NLogger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = LogManager.GetCurrentClassLogger();
                }
                return _logger;
            }
        }

        /// <summary>
        ///     Method that logs debug message
        /// </summary>
        /// <param name="message">The message</param>
        public static void LogDebug(string message)
        {
            NLogger.Debug(message);
        }

        /// <summary>
        ///     Method that logs debug exception and message
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="pException">The exception object to log</param>
        public static void LogDebugException(string message, Exception pException)
        {
            NLogger.Debug(pException, message);
        }

        /// <summary>
        ///     Method that logs error message
        /// </summary>
        /// <param name="message">The message to log</param>
        public static void LogError(string message)
        {
            NLogger.Error(message);
        }

        /// <summary>
        ///     Method that logs error message and exception
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="exception">The exception object to log</param>
        public static void LogErrorException(string message, Exception exception)
        {
            NLogger.Error(exception, message);
        }

        /// <summary>
        ///     Method that logs fatal error message
        /// </summary>
        /// <param name="message">The message to log</param>
        public static void LogFatal(string message)
        {
            NLogger.Fatal(message);
        }

        /// <summary>
        ///     Method that logs fatal error message and exception
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="exception">The exception object to log</param>
        public static void LogFatalException(string message, Exception exception)
        {
            NLogger.Fatal(exception, message);
        }

        /// <summary>
        ///     Method that logs info message
        /// </summary>
        /// <param name="message">The message to log</param>
        public static void LogInfo(string message)
        {
            NLogger.Info(message);
        }

        /// <summary>
        ///     Method that logs info message and exception
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="exception">The exception object to log</param>
        public static void LogInfoException(string message, Exception exception)
        {
            NLogger.Info(exception, message);
        }

        /// <summary>
        ///     Method that logs warn message
        /// </summary>
        /// <param name="exception">The message to log</param>
        public static void LogWarn(string exception)
        {
            NLogger.Warn(exception);
        }

        /// <summary>
        ///     Method that logs warn message and exception
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="exception">The exception object to log</param>
        public static void LogWarnException(string message, Exception exception)
        {
            NLogger.Warn(exception, message);
        }

        /// <summary>
        ///     Method that logs trace message
        /// </summary>
        /// <param name="message">The message to log</param>
        public static void LogTrace(string message)
        {
            NLogger.Trace(message);
        }

        /// <summary>
        ///     Method that logs trace message and exception
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="exception">The exception object to log</param>
        public static void LogTraceException(string message, Exception exception)
        {
            NLogger.Trace(exception, message);
        }
    }
}
