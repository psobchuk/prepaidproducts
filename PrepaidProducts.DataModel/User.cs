﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataModel
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Fullname { get; set; }
        public string CoverImageUrl { get; set; }

        public Nullable<DateTime> Created { get; set; }
        public Nullable<DateTime> Modified { get; set; }

        public RatePlan RatePlan { get; set; }

        public virtual ICollection<UserHistory> UserHistory { get; set; }
    }

    public class UserTypeConfiguration : EntityTypeConfiguration<User>
    {
        public UserTypeConfiguration()
        {
            HasKey(key => key.Id);
            Property(p => p.Email).HasMaxLength(100).IsRequired();
            HasRequired(p => p.RatePlan).WithMany(p => p.Users);
            HasMany(p => p.UserHistory).WithRequired(p => p.User);
        }
    }
}
