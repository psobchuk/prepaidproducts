﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataModel
{
    public class UserHistory
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Product Product { get; set; }
        public Vendor Vendor { get; set; }
        public Nullable<DateTime> Created { get; set; }
    }

    public class UserHistoryTypeConfiguration : EntityTypeConfiguration<UserHistory>
    {
        public UserHistoryTypeConfiguration()
        {
            HasKey(key => key.Id);
        }
    }
}
