﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataModel
{
    public class Vendor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CoverImageName { get; set; }

        public Nullable<DateTime> Created { get; set; }
        public Nullable<DateTime> Modified { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<UserHistory> SellHistory { get; set; }
    }

    public class VendorTypeConfiguration : EntityTypeConfiguration<Vendor>
    {
        public VendorTypeConfiguration()
        {
            HasKey(key => key.Id);
            HasMany(p => p.Products).WithMany(p => p.Vendors);
            HasMany(p => p.SellHistory).WithRequired(p => p.Vendor);
        }
    }
}
