﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataModel
{
    public class RatePlan
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public ICollection<User> Users { get; set; }
    }

    public class RatePlanTypeConfiguration : EntityTypeConfiguration<RatePlan>
    {
        public RatePlanTypeConfiguration()
        {
            HasKey(key => key.Id);
        }
    }
}
