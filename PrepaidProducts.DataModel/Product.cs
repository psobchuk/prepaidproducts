﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataModel
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Vendor> Vendors { get; set; }
        public virtual ICollection<UserHistory> SellHistory { get; set; }
    }

    public class ProductTypeConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductTypeConfiguration()
        {
            HasKey(key => key.Id);
            HasMany(p => p.SellHistory).WithRequired(p => p.Product);
        }
    }
}
