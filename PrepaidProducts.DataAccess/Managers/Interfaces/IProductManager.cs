﻿using PrepaidProducts.DataAccess.DataTransferObjects;
using PrepaidProducts.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess.Managers
{
    public interface IProductManager
    {
        ICollection<ProductDTO> GetAll();
        ProductDTO GetOne(int productId);
        int AddProduct(ProductDTO product);
        void DeleteProduct(int productId);
        ProductDTO UpdateProduct(ProductDTO product);
    }
}
