﻿using PrepaidProducts.DataAccess.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess.Managers
{
    public interface IRatePlanManager
    {
        ICollection<RatePlanDTO> GetAll();
        RatePlanDTO GetOne(int ratePlanId);
        int AddRatePlan(RatePlanDTO ratePlan);
        void DeleteRatePlan(int ratePlanId);
        RatePlanDTO UpdateRatePlan(RatePlanDTO ratePlan);
    }
}
