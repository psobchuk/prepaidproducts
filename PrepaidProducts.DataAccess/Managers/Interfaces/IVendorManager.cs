﻿using PrepaidProducts.DataAccess.DataTransferObjects;
using PrepaidProducts.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess.Managers
{
    public interface IVendorManager
    {
        ICollection<VendorDTO> GetAll();
        VendorDTO GetOne(int vendorId);
        int AddVendor(VendorDTO vendor);
        void DeleteVendor(int vendorId);
        VendorDTO UpdateVendor(VendorDTO vendor);
        void LinkProduct(VendorDTO vendor, ProductDTO product);
        void RemoveLink(int vId, int pId);
        List<VendorDTO> Search(string searchQuery);
    }
}
