﻿using PrepaidProducts.DataAccess.DataTransferObjects;
using PrepaidProducts.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess.Managers
{
    public interface IUserManager
    {
        UserDTO GetUser(int userId);
        UserDTO GetUserByEmail(string email);
        UserDTO UpdateUser(UserDTO user);
        UserDTO InsertUser(UserDTO user);
    }
}
