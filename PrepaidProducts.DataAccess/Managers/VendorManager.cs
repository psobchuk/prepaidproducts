﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidProducts.DataModel;
using PrepaidProducts.DataAccess.DataTransferObjects;
using AutoMapper;

namespace PrepaidProducts.DataAccess.Managers
{
    public class VendorManager : IVendorManager
    {
        private UnitOfWork uof = new UnitOfWork();

        public int AddVendor(VendorDTO vendor)
        {
            var data = Mapper.Map<Vendor>(vendor);

            uof.VendorRepository.Insert(data);
            uof.SaveChanges();

            return vendor.Id;
        }

        public void DeleteVendor(int vendorId)
        {
            uof.VendorRepository.Delete(vendorId);
            uof.SaveChanges();
        }

        public ICollection<VendorDTO> GetAll()
        {
            List<Vendor> vendors = uof.VendorRepository.Get(includeProperties: "Products").ToList();

            return Mapper.Map<List<VendorDTO>>(vendors);
        }

        public VendorDTO GetOne(int vendorId)
        {
            var vendor = uof.VendorRepository.Get(v => v.Id == vendorId, includeProperties: "Products").FirstOrDefault();

            return Mapper.Map<VendorDTO>(vendor);
        }

        public void LinkProduct(VendorDTO vendor, ProductDTO product)
        {
            var vendorData = uof.VendorRepository.Get(v => v.Id == vendor.Id, includeProperties: "Products").FirstOrDefault();
            var productData = uof.ProductRepository.Get(p => p.Id == product.Id).FirstOrDefault();

            vendorData.Products.Add(productData);
            uof.SaveChanges();
        }

        public void RemoveLink(int vId, int pId)
        {
            var vendorData = uof.VendorRepository.Get(v => v.Id == vId, includeProperties: "Products").FirstOrDefault();
            var productData = uof.ProductRepository.Get(p => p.Id == pId).FirstOrDefault();

            vendorData.Products.Remove(productData);
            uof.SaveChanges();
        }

        public List<VendorDTO> Search(string searchQuery)
        {
            var result = uof.VendorRepository.Get(v => v.Address.Contains(searchQuery) || v.Name.Contains(searchQuery));
            return Mapper.Map<List<VendorDTO>>(result.ToList());
        }

        public VendorDTO UpdateVendor(VendorDTO vendor)
        {
            var oldVendor = uof.VendorRepository.Get(v => v.Id == vendor.Id).FirstOrDefault();
            var dataToUpdate = Mapper.Map<Vendor>(vendor);

            uof.VendorRepository.Update(oldVendor, dataToUpdate);
            uof.SaveChanges();

            return vendor;
        }
    }
}
