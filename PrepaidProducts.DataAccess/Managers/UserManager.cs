﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidProducts.DataModel;
using PrepaidProducts.DataAccess.DataTransferObjects;
using AutoMapper;

namespace PrepaidProducts.DataAccess.Managers
{
    public class UserManager : IUserManager
    {
        private UnitOfWork uof = new UnitOfWork();

        public UserDTO GetUserByEmail(string email)
        {
            var user = uof.UserRepository.Get(u => u.Email == email).FirstOrDefault();
            return Mapper.Map<UserDTO>(user);
        }

        public UserDTO GetUser(int userId)
        {
            var user = uof.UserRepository.Get(u => u.Id == userId).FirstOrDefault();
            return Mapper.Map<UserDTO>(user);
        }

        public UserDTO UpdateUser(UserDTO user)
        {
            var dataToUpdate = Mapper.Map<User>(user);
            uof.UserRepository.Update(dataToUpdate, new User());
            uof.SaveChanges();

            return user;
        }

        public UserDTO InsertUser(UserDTO user)
        {
            var data = Mapper.Map<User>(user);
            uof.UserRepository.Insert(data);
            uof.SaveChanges();

            user.Id = data.Id;
            return user;
        }
    }
}
