﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidProducts.DataModel;
using PrepaidProducts.DataAccess.DataTransferObjects;
using AutoMapper;

namespace PrepaidProducts.DataAccess.Managers
{
    public class ProductManager : IProductManager
    {
        private UnitOfWork uof = new UnitOfWork();

        public int AddProduct(ProductDTO product)
        {
            var data = Mapper.Map<Product>(product);
            uof.ProductRepository.Insert(data);
            uof.SaveChanges();

            return product.Id;
        }

        public void DeleteProduct(int productId)
        {
            uof.ProductRepository.Delete(productId);
            uof.SaveChanges();
        }

        public ICollection<ProductDTO> GetAll()
        {
            var data = uof.ProductRepository.Get().ToList();

            return Mapper.Map<List<ProductDTO>>(data);
        }

        public ProductDTO GetOne(int productId)
        {
            var data = uof.ProductRepository.Get(p => p.Id == productId).FirstOrDefault();

            return Mapper.Map<ProductDTO>(data);
            
        }

        public ProductDTO UpdateProduct(ProductDTO product)
        {
            var oldProduct = uof.ProductRepository.Get(v => v.Id == product.Id).FirstOrDefault();
            var dataToUpdate = Mapper.Map<Product>(product);

            uof.ProductRepository.Update(oldProduct, dataToUpdate);
            uof.SaveChanges();

            return product;
        }
    }
}
