﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrepaidProducts.DataAccess.DataTransferObjects;
using AutoMapper;
using PrepaidProducts.DataModel;

namespace PrepaidProducts.DataAccess.Managers
{
    public class RatePlanManager : IRatePlanManager
    {
        private UnitOfWork uof = new UnitOfWork();

        public int AddRatePlan(RatePlanDTO ratePlan)
        {
            var data = Mapper.Map<RatePlan>(ratePlan);
            uof.RatePlanRepository.Insert(data);
            uof.SaveChanges();

            return data.Id;
        }

        public void DeleteRatePlan(int ratePlanId)
        {
            uof.RatePlanRepository.Delete(ratePlanId);
            uof.SaveChanges();
        }

        public ICollection<RatePlanDTO> GetAll()
        {
            var data = uof.RatePlanRepository.Get().ToList();
            return Mapper.Map<List<RatePlanDTO>>(data);
        }

        public RatePlanDTO GetOne(int ratePlanId)
        {
            var data = uof.UserRepository.Get(u => u.Id == ratePlanId).FirstOrDefault();
            return Mapper.Map<RatePlanDTO>(data);
        }

        public RatePlanDTO UpdateRatePlan(RatePlanDTO ratePlan)
        {
            var oldRatePlan = uof.RatePlanRepository.Get(v => v.Id == ratePlan.Id).FirstOrDefault();
            var dataToUpdate = Mapper.Map<RatePlan>(ratePlan);

            uof.RatePlanRepository.Update(oldRatePlan, dataToUpdate);
            uof.SaveChanges();

            return ratePlan;
        }
    }
}
