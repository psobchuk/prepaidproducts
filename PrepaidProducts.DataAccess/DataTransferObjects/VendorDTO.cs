﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess.DataTransferObjects
{
    public class VendorDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CoverImageName { get; set; }

        public string ImageUrl { get; set; }

        public Nullable<DateTime> Created { get; set; }
        public Nullable<DateTime> Modified { get; set; }

        public ICollection<ProductDTO> Products { get; set; }

        public VendorDTO()
        {
            Products = new List<ProductDTO>();
        }
    }
}
