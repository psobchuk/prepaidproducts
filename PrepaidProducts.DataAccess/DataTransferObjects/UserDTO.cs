﻿using PrepaidProducts.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess.DataTransferObjects
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }

        public Nullable<DateTime> Created { get; set; }
        public Nullable<DateTime> Modified { get; set; }

        public RatePlanDTO RatePlan { get; set; }
    }
}
