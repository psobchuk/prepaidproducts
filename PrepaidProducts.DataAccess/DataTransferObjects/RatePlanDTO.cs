﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess.DataTransferObjects
{
    public class RatePlanDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public ICollection<UserDTO> Users { get; set; }

        public RatePlanDTO()
        {
            Users = new List<UserDTO>();
        }
    }
}
