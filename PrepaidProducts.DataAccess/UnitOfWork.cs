﻿using PrepaidProducts.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess
{
    public class UnitOfWork
    {
        #region Private Fields

        private ApplicationDbContext context = new ApplicationDbContext();
        private UserRepository userRepository;
        private VendorRepository vendorRepository;
        private ProductRepository productRepository;
        private RatePlanRepository ratePlanRepository;

        #endregion

        #region Public Properties

        public UserRepository UserRepository
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(context);
                }

                return userRepository;
            }
        }
        public VendorRepository VendorRepository
        {
            get
            {
                if (vendorRepository == null)
                {
                    vendorRepository = new VendorRepository(context);
                }

                return vendorRepository;
            }
        }
        public ProductRepository ProductRepository
        {
            get
            {
                if (productRepository == null)
                {
                    productRepository = new ProductRepository(context);
                }

                return productRepository;
            }
        }
        public RatePlanRepository RatePlanRepository
        {
            get
            {
                if (ratePlanRepository == null)
                {
                    ratePlanRepository = new RatePlanRepository(context);
                }

                return ratePlanRepository;
            }
        }

        #endregion

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}
