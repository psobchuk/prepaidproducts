﻿using PrepaidProducts.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess.Repositories
{
    public class ProductRepository: Repository<Product>
    {
        public ProductRepository(ApplicationDbContext context) : base(context) { }
    }
}
