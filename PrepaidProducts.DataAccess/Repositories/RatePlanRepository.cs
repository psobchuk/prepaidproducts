﻿using PrepaidProducts.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrepaidProducts.DataAccess.Repositories
{
    public class RatePlanRepository: Repository<RatePlan>
    {
        public RatePlanRepository(ApplicationDbContext context): base(context) { }
    }
}
