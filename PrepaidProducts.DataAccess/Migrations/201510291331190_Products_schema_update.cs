namespace PrepaidProducts.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Products_schema_update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "Price", c => c.Double(nullable: false));
            AddColumn("dbo.Products", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "Description");
            DropColumn("dbo.Products", "Price");
        }
    }
}
