namespace PrepaidProducts.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Address = c.String(),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        Created = c.DateTime(),
                        Modified = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RatePlans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Users",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Email = c.String(nullable: false, maxLength: 100),
                    Password = c.String(),
                    Fullname = c.String(),
                    Created = c.DateTime(),
                    Modified = c.DateTime(),
                    RatePlan_Id = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RatePlans", t => t.RatePlan_Id, cascadeDelete: false)
                .Index(t => t.RatePlan_Id);

            CreateTable(
                "dbo.VendorProducts",
                c => new
                {
                    Vendor_Id = c.Int(nullable: false),
                    Product_Id = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.Vendor_Id, t.Product_Id })
                .ForeignKey("dbo.Vendors", t => t.Vendor_Id, cascadeDelete: false)
                .ForeignKey("dbo.Products", t => t.Product_Id, cascadeDelete: false)
                .Index(t => t.Vendor_Id)
                .Index(t => t.Product_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "RatePlan_Id", "dbo.RatePlans");
            DropForeignKey("dbo.VendorProducts", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.VendorProducts", "Vendor_Id", "dbo.Vendors");
            DropIndex("dbo.VendorProducts", new[] { "Product_Id" });
            DropIndex("dbo.VendorProducts", new[] { "Vendor_Id" });
            DropIndex("dbo.Users", new[] { "RatePlan_Id" });
            DropTable("dbo.VendorProducts");
            DropTable("dbo.Users");
            DropTable("dbo.RatePlans");
            DropTable("dbo.Vendors");
            DropTable("dbo.Products");
        }
    }
}
