namespace PrepaidProducts.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Vendor_cover_image_name : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vendors", "CoverImageName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vendors", "CoverImageName");
        }
    }
}
