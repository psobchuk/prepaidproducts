namespace PrepaidProducts.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class user_history : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Created = c.DateTime(),
                        User_Id = c.Int(nullable: false),
                        Vendor_Id = c.Int(nullable: false),
                        Product_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: false)
                .ForeignKey("dbo.Vendors", t => t.Vendor_Id, cascadeDelete: false)
                .ForeignKey("dbo.Products", t => t.Product_Id, cascadeDelete: false)
                .Index(t => t.User_Id)
                .Index(t => t.Vendor_Id)
                .Index(t => t.Product_Id);
            
            AddColumn("dbo.Users", "CoverImageUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserHistories", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.UserHistories", "Vendor_Id", "dbo.Vendors");
            DropForeignKey("dbo.UserHistories", "User_Id", "dbo.Users");
            DropIndex("dbo.UserHistories", new[] { "Product_Id" });
            DropIndex("dbo.UserHistories", new[] { "Vendor_Id" });
            DropIndex("dbo.UserHistories", new[] { "User_Id" });
            DropColumn("dbo.Users", "CoverImageUrl");
            DropTable("dbo.UserHistories");
        }
    }
}
