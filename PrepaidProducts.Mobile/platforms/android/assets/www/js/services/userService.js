﻿PPMobile.factory('UserSvc', ["$http", 'urls', function ($http, urls) {
    return {
        register: function (data, fnSuccess, fnError) {
            $http.post(urls.apiUrl + "/account/register", data).success(fnSuccess).error(fnError);
        },

        getUserData: function (fnSuccess, fnError) {
            $http.get(urls.apiUrl + "/account/getuser").success(fnSuccess).error(fnError);
        },

        login: function (data, fnSuccess, fnError) {
            $http.post(urls.apiUrl + "/account/login", data).success(fnSuccess).error(fnError);
        },

        recoverPassword: function (email, fnSuccess, fnError) {
            $http.post(urls.apiUrl + "/account/recoverpassword", email).success(fnSuccess).error(fnError);
        }
    };
}]);