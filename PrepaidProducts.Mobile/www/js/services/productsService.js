﻿PPMobile.factory('ProductSvc', ["$http", 'urls', function ($http, urls) {
    return {
        getVendorProducts: function (vendorId, fnSuccess, fnError) {
            $http.get(urls.apiUrl + "/products/" + vendorId).success(fnSuccess).error(fnError);
        },

        selectProduct: function (productId, userId, fnSuccess, fnError) {
            $http.post(urls.apiUrl + '/products/select', { pId: productId, uId: userId }).success(fnSuccess).error(fnError);
        }
    };
}]);