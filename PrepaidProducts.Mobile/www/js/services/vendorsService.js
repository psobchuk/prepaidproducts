﻿PPMobile.factory('VendorSvc', ["$http", 'urls', function ($http, urls) {
    return {
        loadVendors: function (fnSuccess, fnError) {
            $http.get(urls.apiUrl + '/vendors/load').success(fnSuccess).error(fnError);
        },

        searchVendors: function (query, fnSuccess, fnError) {
            $http.get(urls.apiUrl + '/vendors/search?sq=' + query).success(fnSuccess).error(fnError);
        }
    };
}]);